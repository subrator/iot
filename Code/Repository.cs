﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using iot.Model;
using Newtonsoft.Json.Linq;
using System.Net;

namespace iot
{
    class Repository
    {
        private static string connectionstring = null;
        private static NpgsqlConnection db_connection = null;
        private static Boolean isopen = false;
        private static Logger logger = new Logger(); 
        public Repository()
        {
            var DBHost = ConfigurationManager.AppSettings["DBHost"];
            var DBPort = ConfigurationManager.AppSettings["DBPort"];
            var DBName = ConfigurationManager.AppSettings["DBName"];
            var DBUser = ConfigurationManager.AppSettings["DBUser"];
            var DBPass = ConfigurationManager.AppSettings["DBPass"];

            connectionstring = String.Format("Server={0};Port={1};Database={2};User Id={3};Password={4};", DBHost, DBPort, DBName, DBUser, DBPass);
        }

        private static DateTime UnixTimeStampToDateTime(long unixTimeStamp)
        {
            System.DateTime dtDateTime = DateTime.Now;
            bool converted = false;

            try
            {
                dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
                converted = true;
            }
            catch (Exception ex)
            {
                converted = false;
            }

            if (!converted)
            {
                try
                {
                    dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                    dtDateTime = dtDateTime.AddMilliseconds(unixTimeStamp).ToLocalTime();
                    converted = true;
                }
                catch (Exception ex)
                {
                    converted = false;
                }
            }

            return dtDateTime;
        }

        public Boolean Open(ref String statusstring)
        {
            try
            {
                db_connection = new NpgsqlConnection(connectionstring);
                db_connection.Open();

                statusstring = "Database opened";

                isopen = true;
                return true;
            }
            catch (Exception ex)
            {
                if (ex != null)
                {
                    statusstring = ex.Message;

                    if (ex.InnerException != null)
                    {
                        statusstring += "\n\n";
                        statusstring += ex.InnerException.Message;
                    }
                }

            }
            return false;
        }
        public virtual Boolean IsOpen
        {
            get
            {
                return isopen;
            }
        }

        public SystemInformation GetSystemInformation() { return new SystemInformation(); }

        public Device[] GetDevices(String clientid = null, Int32 pagesize = 10, Int32 pageno = 1)
        {
            if (db_connection.State != ConnectionState.Open)
            {
                logger.Write("Connection is not open, reopening");
                db_connection.Open();
            }

            Int32 offset = pagesize * (pageno - 1);

            string command_string = null;

            command_string += "SELECT device_id, \"location\", device_name, latitude, longitude, active_ip_address, is_deleted FROM public.device_clients ORDER BY \"device_id\" ASC";
            command_string += " LIMIT " + Convert.ToString(pagesize);
            command_string += " OFFSET " + Convert.ToString(offset);

            if (clientid != null)
            {
                command_string += " WHERE  \"client_id\" = ";
                command_string += "'" + clientid + "'";
            }

            List<Device> fetchedrows = new List<Device>();

            NpgsqlCommand command = new NpgsqlCommand(command_string);
            command.Connection = db_connection;
            NpgsqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Device row = new Device();

                    row.DeviceId = Convert.ToString(reader[0]);
                    row.Location = Convert.ToString(reader[1]);
                    row.DeviceName = Convert.ToString(reader[2]);
                    row.Latitude = Convert.ToDouble(reader[3]);
                    row.Longitude = Convert.ToDouble(reader[4]);
                    row.ActiveIpAddress = Convert.ToString(reader[5]);
                    row.IsDeleted = Convert.ToBoolean(reader[6]);

                    fetchedrows.Add(row);
                }
            }

            reader.Close();
            command.Dispose();

            return fetchedrows.ToArray();
        }

        public Peripheral[] GetPeripherals(String deviceid = null, Int32 pagesize = 10, Int32 pageno = 1)
        {
            if (db_connection.State != ConnectionState.Open)
            {
                logger.Write("Connection is not open, reopening");
                db_connection.Open();
            }

            Int32 offset = pagesize * (pageno - 1);

            string command_string = null;

            command_string += "select * from device_peripheral_mapping ORDER BY \"device_id\" ASC";
            command_string += " LIMIT " + Convert.ToString(pagesize);
            command_string += " OFFSET " + Convert.ToString(offset);

            if (deviceid != null)
            {
                command_string += " WHERE  \"device_id\" = ";
                command_string += "'" + deviceid + "'";
            }

            List<Peripheral> fetchedrows = new List<Peripheral>();

            NpgsqlCommand command = new NpgsqlCommand(command_string);
            command.Connection = db_connection;
            NpgsqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Peripheral row = new Peripheral();

                    row.DeviceId = Convert.ToString(reader[0]);
                    row.PeripheralId = Convert.ToString(reader[1]);
                    row.PeripheralTypeId = Convert.ToInt32(reader[2]);
                    row.PeripheralName = Convert.ToString(reader[3]);
                    row.IoType = Convert.ToString(reader[4]);
                    row.PeripheralType = Convert.ToString(reader[5]);

                    fetchedrows.Add(row);
                }
            }

            reader.Close();
            command.Dispose();

            return fetchedrows.ToArray();

        }

        public DeviceEvent[] GetDeviceEvents(String deviceid, Int32 pagesize = 10, Int32 pageno = 1)
        {
            if (db_connection.State != ConnectionState.Open)
            {
                logger.Write("Connection is not open, reopening");
                db_connection.Open();
            }

            Int32 offset = pagesize * (pageno - 1);

            string command_string = null;

            command_string += "SELECT * FROM public.event_history ORDER BY \"sensor_timestamp\" ASC";
            command_string += " LIMIT " + Convert.ToString(pagesize);
            command_string += " OFFSET " + Convert.ToString(offset);

            if (deviceid != null)
            {
                command_string += " WHERE  \"edge_device_id\" = ";
                command_string += "'" + deviceid + "'";
            }

            List<DeviceEvent> fetchedrows = new List<DeviceEvent>();

            NpgsqlCommand command = new NpgsqlCommand(command_string);
            command.Connection = db_connection;
            NpgsqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    DeviceEvent row = new DeviceEvent();

                    row.EdgeDeviceId = Convert.ToString(reader[0]);
                    row.SensorId = Convert.ToString(reader[1]);
                    row.EventName = Convert.ToString(reader[2]);
                    row.EventParameter = Convert.ToString(reader[3]);
                    row.SensorTimestamp = Convert.ToDateTime(reader[4]);
                    row.EventSource = Convert.ToString(reader[5]);

                    fetchedrows.Add(row);
                }
            }

            reader.Close();
            command.Dispose();

            return fetchedrows.ToArray();
        }

        public TelemetryPublicWiFi[] GetTelemetryPublicWiFi(String deviceid, Int32 pagesize = 10, Int32 pageno = 1)
        {
            if (db_connection.State != ConnectionState.Open)
            {
                logger.Write("Connection is not open, reopening");
                db_connection.Open();
            }

            Int32 offset = pagesize * (pageno - 1);

            string command_string = null;

            command_string += "select * from telemetry_public_wifi_view ORDER BY \"sensor_timestamp\" ASC";
            command_string += " LIMIT " + Convert.ToString(pagesize);
            command_string += " OFFSET " + Convert.ToString(offset);

            if (deviceid != null)
            {
                command_string += " WHERE  \"edge_device_id\" = ";
                command_string += "'" + deviceid + "'";
            }

            List<TelemetryPublicWiFi> fetchedrows = new List<TelemetryPublicWiFi>();

            NpgsqlCommand command = new NpgsqlCommand(command_string);
            command.Connection = db_connection;
            NpgsqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    TelemetryPublicWiFi row = new TelemetryPublicWiFi();

                    row.UserConnectedCount = Convert.ToInt32(reader[0]);
                    row.DataUpload = Convert.ToDouble(reader[1]);
                    row.DataUploadMB = Convert.ToDouble(reader[2]);
                    row.DataUploadMBPS = Convert.ToDouble(reader[3]);
                    row.DataDownload = Convert.ToDouble(reader[4]);
                    row.DataDownloadMB = Convert.ToDouble(reader[5]);
                    row.DataDownloadMBPS = Convert.ToDouble(reader[6]);
                    row.SensorTimestamp = Convert.ToDateTime(reader[7]);
                    row.EdgeDeviceId = Convert.ToString(reader[8]);
                    row.SensorId = Convert.ToString(reader[9]);
                    row.SamplingRate = Convert.ToInt32(reader[10]);

                    fetchedrows.Add(row);
                }
            }

            reader.Close();
            command.Dispose();

            return fetchedrows.ToArray();
        }

        public TelemetryEnvironment[] GetTelemetryEnvironment(String deviceid, Int32 pagesize = 10, Int32 pageno = 1)
        {
            if (db_connection.State != ConnectionState.Open)
            {
                logger.Write("Connection is not open, reopening");
                db_connection.Open();
            }

            Int32 offset = pagesize * (pageno - 1);

            string command_string = null;

            command_string += "select * from telemetry_environment ORDER BY \"sensor_timestamp\" ASC";

            command_string += " LIMIT " + Convert.ToString(pagesize);
            command_string += " OFFSET " + Convert.ToString(offset);

            if (deviceid != null)
            {
                command_string += " WHERE  \"edge_device_id\" = ";
                command_string += "'" + deviceid + "'";
            }

            List<TelemetryEnvironment> fetchedrows = new List<TelemetryEnvironment>();

            NpgsqlCommand command = new NpgsqlCommand(command_string);
            command.Connection = db_connection;
            NpgsqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    TelemetryEnvironment row = new TelemetryEnvironment();

                    row.PM25 = Convert.ToDouble(reader[0]);
                    row.PM10 = Convert.ToDouble(reader[1]);
                    row.RelativeHumidity = Convert.ToDouble(reader[2]);
                    row.DewPoint = Convert.ToDouble(reader[3]);
                    row.NO2 = Convert.ToDouble(reader[4]);
                    row.SO2 = Convert.ToDouble(reader[5]);
                    row.O3 = Convert.ToDouble(reader[6]);
                    row.CO2 = Convert.ToDouble(reader[7]);
                    row.SensorTimestamp = Convert.ToDateTime(reader[8]);
                    row.Temperature = Convert.ToDouble(reader[9]);
                    row.CO = Convert.ToDouble(reader[10]);
                    row.EdgeDeviceId = Convert.ToString(reader[11]);
                    row.SensorId = Convert.ToString(reader[12]);

                    fetchedrows.Add(row);
                }
            }

            reader.Close();
            command.Dispose();

            return fetchedrows.ToArray();
        }

        public TelemetryLighting[] GetTelemetryLighting(String deviceid, Int32 pagesize = 10, Int32 pageno = 1)
        {
            if (db_connection.State != ConnectionState.Open)
            {
                logger.Write("Connection is not open, reopening");
                db_connection.Open();
            }

            Int32 offset = pagesize * (pageno - 1);

            string command_string = null;

            command_string += "select * from telemetry_lighting ORDER BY \"sensor_timestamp\" ASC";

            command_string += " LIMIT " + Convert.ToString(pagesize);
            command_string += " OFFSET " + Convert.ToString(offset);

            if (deviceid != null)
            {
                command_string += " WHERE  \"edge_device_id\" = ";
                command_string += "'" + deviceid + "'";
            }

            List<TelemetryLighting> fetchedrows = new List<TelemetryLighting>();

            NpgsqlCommand command = new NpgsqlCommand(command_string);
            command.Connection = db_connection;
            NpgsqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    TelemetryLighting row = new TelemetryLighting();

                    row.Voltage = Convert.ToDouble(reader[0]);
                    row.Current = Convert.ToDouble(reader[1]);
                    row.EnergyKwh = Convert.ToDouble(reader[2]);
                    row.BurnHours = Convert.ToDouble(reader[3]);
                    row.Dimming = Convert.ToDouble(reader[4]);
                    row.PowerFactor = Convert.ToDouble(reader[5]);
                    row.LampMode = Convert.ToInt32(reader[6]);
                    row.SensorTimestamp = Convert.ToDateTime(reader[7]);
                    row.DigitalInput = Convert.ToInt32(reader[8]);
                    row.AnalogInput = Convert.ToInt32(reader[9]);
                    row.Db = Convert.ToInt32(reader[10]);
                    row.PowerWatts = Convert.ToDouble(reader[11]);
                    row.Temperature = Convert.ToDouble(reader[12]);
                    row.LampSteadyCurrent = Convert.ToDouble(reader[13]);
                    row.Reset = Convert.ToInt32(reader[14]);
                    row.ResetInfo = Convert.ToInt32(reader[15]);
                    row.EdgeDeviceId = Convert.ToString(reader[16]);
                    row.Di01 = Convert.ToInt32(reader[17]);
                    row.Di02 = Convert.ToInt32(reader[18]);
                    row.Di03 = Convert.ToInt32(reader[19]);
                    row.Di04 = Convert.ToInt32(reader[20]);
                    row.Di05 = Convert.ToInt32(reader[21]);
                    row.Di06 = Convert.ToInt32(reader[22]);
                    row.Di07 = Convert.ToInt32(reader[23]);
                    row.Di08 = Convert.ToInt32(reader[24]);
                    row.Di09 = Convert.ToInt32(reader[25]); 
                    row.Di10 = Convert.ToInt32(reader[26]);
                    row.Di11 = Convert.ToInt32(reader[27]);
                    row.Di12 = Convert.ToInt32(reader[28]);
                    row.Di13 = Convert.ToInt32(reader[29]);
                    row.Di14 = Convert.ToInt32(reader[30]);
                    row.Di15 = Convert.ToInt32(reader[31]);
                    row.Di16 = Convert.ToInt32(reader[32]);
                    row.SensorId = Convert.ToString(reader[33]);
                    row.Identifier = Convert.ToString(reader[34]);

                    fetchedrows.Add(row);
                }
            }

            reader.Close();
            command.Dispose();

            return fetchedrows.ToArray();
        }

        static public HttpStatusCode AddTelemetryPublicWifi(TelemetryPublicWiFi item)
        {
            try
            {
                if(db_connection.State != ConnectionState.Open)
                {
                    logger.Write("Connection is not open, reopening");
                    db_connection.Open();
                }

                string command_string = null;

                command_string += "INSERT INTO public.telemetry_public_wifi (users_connected_count, data_upload, data_download, sensor_timestamp, edge_device_id, sensor_id, sampling_rate) VALUES( <users_connected_count>, <data_upload>, <data_download>, '<sensor_timestamp>', '<edge_device_id>', '<sensor_id>', <sampling_rate> )";

                command_string = command_string.Replace("<users_connected_count>", Convert.ToString(item.UserConnectedCount));
                command_string = command_string.Replace("<data_upload>", Convert.ToString(item.DataUpload));
                command_string = command_string.Replace("<data_download>", Convert.ToString(item.DataDownload));
                command_string = command_string.Replace("<sampling_rate>", Convert.ToString(item.SamplingRate));

                command_string = command_string.Replace("<edge_device_id>", item.EdgeDeviceId);
                command_string = command_string.Replace("<sensor_id>", item.SensorId);

                command_string = command_string.Replace("<sensor_timestamp>", item.SensorTimestamp.ToString("yyyy-MM-dd HH:mm:ss"));

                NpgsqlCommand command = new NpgsqlCommand(command_string);
                command.Connection = db_connection;
                int rows = command.ExecuteNonQuery();

                if(rows > 0)
                {
                    logger.Write("Inserted public wifi telementry");
                }

                return HttpStatusCode.NoContent;
            }
            catch (Exception ex)
            {
                logger.Write(ex.Message);
            }
            return HttpStatusCode.BadRequest;
        }

        static public HttpStatusCode AddTelemetryEnvironment(TelemetryEnvironment item)
        {
            try
            {
                if (db_connection.State != ConnectionState.Open)
                {
                    logger.Write("Connection is not open, reopening");
                    db_connection.Open();
                }

                string command_string = null;

                command_string += "INSERT INTO public.telemetry_environment (pm25, pm10, relative_humidity, no2, so2, co2, co, dew_point, o3, temperature, sensor_timestamp, edge_device_id, sensor_id) VALUES( <pm25>, <pm10>, <relative_humidity>, <no2>, <so2>, <co2>, <co>, 0.0, 0.0, <temperature>, '<sensor_timestamp>', '<edge_device_id>', '<sensor_id>')";

                command_string = command_string.Replace("<pm25>", Convert.ToString(item.PM25));
                command_string = command_string.Replace("<pm10>", Convert.ToString(item.PM10));
                command_string = command_string.Replace("<relative_humidity>", Convert.ToString(item.RelativeHumidity));
                command_string = command_string.Replace("<no2>", Convert.ToString(item.NO2));
                command_string = command_string.Replace("<so2>", Convert.ToString(item.SO2));
                command_string = command_string.Replace("<co2>", Convert.ToString(item.CO2));
                command_string = command_string.Replace("<co>", Convert.ToString(item.CO));
                command_string = command_string.Replace("<temperature>", Convert.ToString(item.Temperature));

                command_string = command_string.Replace("<edge_device_id>", item.EdgeDeviceId);
                command_string = command_string.Replace("<sensor_id>", item.SensorId);
                command_string = command_string.Replace("<sensor_timestamp>", item.SensorTimestamp.ToString("yyyy-MM-dd HH:mm:ss"));

                NpgsqlCommand command = new NpgsqlCommand(command_string);
                command.Connection = db_connection;
                int rows = command.ExecuteNonQuery();

                if (rows > 0)
                {
                    logger.Write("Inserted environment telementry");
                }

                return HttpStatusCode.NoContent;
            }
            catch (Exception ex)
            {
                logger.Write(ex.Message);
            }
            return HttpStatusCode.BadRequest;
        }

        static public HttpStatusCode AddTelemetryLighting(TelemetryLighting item)
        {
            try
            {
                if (db_connection.State != ConnectionState.Open)
                {
                    logger.Write("Connection is not open, reopening");
                    db_connection.Open();
                }

                string command_string = null;

                command_string += "INSERT INTO public.telemetry_lighting ( voltage, \"current\", energy_kwh, burn_hours, dimming, power_factor, lamp_mode, sensor_timestamp, di, ai, db, power_watts, temperature, lsc, \"reset\", reset_info, edge_device_id, di01, di02, di03, di04, di05, di06, di07, di08, di09, di10, di11, di12, di13, di14, di15, di16, sensor_id, identifier) VALUES(<voltage>, <current>, <energy_kwh>, <burn_hours>, <dimming>, <power_factor>, <lamp_mode>, '<sensor_timestamp>', <di>, <ai>, <db>, <power_watts>, <temperature>, <lsc>, <reset>, <reset_info>, '<edge_device_id>', <di01>, <di02>, <di03>, <di04>, <di05>, <di06>, <di07>, <di08>, <di09>, <di10>, <di11>, <di12>, <di13>, <di14>, <di15>, <di16>, '<sensor_id>', '<identifier>')";

                command_string = command_string.Replace("<voltage>", Convert.ToString(item.Voltage));
                command_string = command_string.Replace("<current>", Convert.ToString(item.Current));
                command_string = command_string.Replace("<energy_kwh>", Convert.ToString(item.EnergyKwh));
                command_string = command_string.Replace("<burn_hours>", Convert.ToString(item.BurnHours));
                command_string = command_string.Replace("<dimming>", Convert.ToString(item.Dimming));
                command_string = command_string.Replace("<power_factor>", Convert.ToString(item.PowerFactor));
                command_string = command_string.Replace("<lamp_mode>", Convert.ToString(item.LampMode));
                command_string = command_string.Replace("<di>", Convert.ToString(item.DigitalInput));
                command_string = command_string.Replace("<ai>", Convert.ToString(item.AnalogInput));
                command_string = command_string.Replace("<db>", Convert.ToString(item.Db));
                command_string = command_string.Replace("<power_watts>", Convert.ToString(item.PowerWatts));
                command_string = command_string.Replace("<temperature>", Convert.ToString(item.Temperature));
                command_string = command_string.Replace("<lsc>", Convert.ToString(item.LampSteadyCurrent));
                command_string = command_string.Replace("<reset>", Convert.ToString(item.Reset));
                command_string = command_string.Replace("<reset_info>", Convert.ToString(item.ResetInfo));
                command_string = command_string.Replace("<di01>", Convert.ToString(item.Di01));
                command_string = command_string.Replace("<di02>", Convert.ToString(item.Di02));
                command_string = command_string.Replace("<di03>", Convert.ToString(item.Di03));
                command_string = command_string.Replace("<di04>", Convert.ToString(item.Di04));
                command_string = command_string.Replace("<di05>", Convert.ToString(item.Di05));
                command_string = command_string.Replace("<di06>", Convert.ToString(item.Di06));
                command_string = command_string.Replace("<di07>", Convert.ToString(item.Di07));
                command_string = command_string.Replace("<di08>", Convert.ToString(item.Di08));
                command_string = command_string.Replace("<di09>", Convert.ToString(item.Di09));
                command_string = command_string.Replace("<di10>", Convert.ToString(item.Di10));
                command_string = command_string.Replace("<di11>", Convert.ToString(item.Di11));
                command_string = command_string.Replace("<di12>", Convert.ToString(item.Di12));
                command_string = command_string.Replace("<di13>", Convert.ToString(item.Di13));
                command_string = command_string.Replace("<di14>", Convert.ToString(item.Di14));
                command_string = command_string.Replace("<di15>", Convert.ToString(item.Di15));
                command_string = command_string.Replace("<di16>", Convert.ToString(item.Di16));

                command_string = command_string.Replace("<identifier>", item.Identifier);
                command_string = command_string.Replace("<edge_device_id>", item.EdgeDeviceId);
                command_string = command_string.Replace("<sensor_id>", item.SensorId);
                command_string = command_string.Replace("<sensor_timestamp>", item.SensorTimestamp.ToString("yyyy-MM-dd HH:mm:ss"));

                NpgsqlCommand command = new NpgsqlCommand(command_string);
                command.Connection = db_connection;
                int rows = command.ExecuteNonQuery();

                if (rows > 0)
                {
                    logger.Write("Inserted lighting telementry");
                }

                return HttpStatusCode.NoContent;
            }
            catch (Exception ex)
            {
                logger.Write(ex.Message);
            }
            return HttpStatusCode.BadRequest;
        }

        static public void UpdateDeviceInfo(Device item)
        {
        }

        static public HttpStatusCode HandleAlert(string postdata, string clientipaddress)
        {
            postdata = postdata.ToLower();

            JObject docobj = JObject.Parse(postdata);

            if (docobj == null)
            {
                return HttpStatusCode.BadRequest;
            }

            var root = docobj.GetValue("records");

            if (root == null)
            {
                return HttpStatusCode.BadRequest;
            }

            foreach (var header in root.Children())
            {
                if (header != null)
                {
                    var paylaod = header.SelectToken("payload").First;
                    var edge_device_id = header.Value<String>("nsid").ToUpper();

                    if (paylaod != null)
                    {
                        return HandleAlertPayload(paylaod, edge_device_id, clientipaddress);
                    }
                }
            }

            return HttpStatusCode.BadRequest;

        }
        static public HttpStatusCode HandlePost(string postdata, string clientipaddress)
        {
            postdata = postdata.ToLower();

            JObject docobj = JObject.Parse(postdata);

            if (docobj == null)
            {
                return HttpStatusCode.BadRequest;
            }

            var root = docobj.GetValue("records");

            if (root == null)
            {
                return HttpStatusCode.BadRequest;
            }

            foreach(var header in root.Children())
            {
                if (header != null)
                {
                    var paylaod = header.SelectToken("payload").First;
                    var edge_device_id = header.Value<String>("nsid").ToUpper();

                    if (paylaod != null)
                    {
                        return HandleTelemetryPayload(paylaod, edge_device_id, clientipaddress);
                    }
                }
            }

            return HttpStatusCode.BadRequest;
        }
        static private HttpStatusCode HandleTelemetryPayload(JToken token, String edge_device_id, string clientipaddress)
        {
            if (token.SelectToken("co2") != null)
            {
                TelemetryEnvironment env = new TelemetryEnvironment();
                env.CO2 = token.Value<double>("co2");
                env.PM25 = token.Value<double>("pm2.5");
                env.PM10 = token.Value<double>("pm10");
                env.CO = token.Value<double>("co");
                env.SO2 = token.Value<double>("so2");
                env.NO2 = token.Value<double>("no2");
                env.Temperature = token.Value<double>("temperature");
                env.RelativeHumidity = token.Value<double>("humidity");
                env.RelativeHumidity = token.Value<double>("humidity");
                env.EdgeDeviceId = edge_device_id;
                env.SensorId = token.Value<String>("slaveid");

                Int64 epoch_offset = token.Value<Int64>("timestamp");
                env.SensorTimestamp = UnixTimeStampToDateTime(epoch_offset);

                return Repository.AddTelemetryEnvironment(env);
            }
            else
            {
                if (token.SelectToken("burn_hrs") != null)
                {
                    TelemetryLighting lgt = new TelemetryLighting();
                    lgt.AnalogInput = token.Value<Int32>("ai");
                    lgt.BurnHours = token.Value<double>("burn_hrs");
                    lgt.Current = token.Value<double>("current");
                    lgt.Db = token.Value<Int32>("db");
                    lgt.DigitalInput = token.Value<Int32>("di");

                    int ctr = 0;

                    foreach (var di_value in token.SelectToken("di_value"))
                    {
                        ctr++;

                        if(ctr > 0 && ctr < 17)
                        {
                            switch(ctr)
                            {
                                case 1:
                                    {
                                        lgt.Di01 = Convert.ToInt32(di_value.ToString());
                                        break;
                                    }
                                case 2:
                                    {
                                        lgt.Di02 = Convert.ToInt32(di_value.ToString());
                                        break;
                                    }
                                case 3:
                                    {
                                        lgt.Di03 = Convert.ToInt32(di_value.ToString());
                                        break;
                                    }
                                case 4:
                                    {
                                        lgt.Di04 = Convert.ToInt32(di_value.ToString());
                                        break;
                                    }
                                case 5:
                                    {
                                        lgt.Di05 = Convert.ToInt32(di_value.ToString());
                                        break;
                                    }
                                case 6:
                                    {
                                        lgt.Di06 = Convert.ToInt32(di_value.ToString());
                                        break;
                                    }
                                case 7:
                                    {
                                        lgt.Di07 = Convert.ToInt32(di_value.ToString());
                                        break;
                                    }
                                case 8:
                                    {
                                        lgt.Di08 = Convert.ToInt32(di_value.ToString());
                                        break;
                                    }
                                case 9:
                                    {
                                        lgt.Di09 = Convert.ToInt32(di_value.ToString());
                                        break;
                                    }
                                case 10:
                                    {
                                        lgt.Di10 = Convert.ToInt32(di_value.ToString());
                                        break;
                                    }
                                case 11:
                                    {
                                        lgt.Di11 = Convert.ToInt32(di_value.ToString());
                                        break;
                                    }
                                case 12:
                                    {
                                        lgt.Di12 = Convert.ToInt32(di_value.ToString());
                                        break;
                                    }
                                case 13:
                                    {
                                        lgt.Di13 = Convert.ToInt32(di_value.ToString());
                                        break;
                                    }
                                case 14:
                                    {
                                        lgt.Di14 = Convert.ToInt32(di_value.ToString());
                                        break;
                                    }
                                case 15:
                                    {
                                        lgt.Di15 = Convert.ToInt32(di_value.ToString());
                                        break;
                                    }
                                case 16:
                                    {
                                        lgt.Di16 = Convert.ToInt32(di_value.ToString());
                                        break;
                                    }
                                default:
                                    break;
                            }
                        }

                    }

                    lgt.Dimming = token.Value<double>("dimming");
                    lgt.EnergyKwh = token.Value<double>("kwh");
                    lgt.LampMode = token.Value<Int32>("mode");
                    lgt.LampSteadyCurrent = token.Value<Int32>("lsc");
                    lgt.PowerFactor = token.Value<double>("pf");
                    lgt.PowerWatts = token.Value<double>("watts");
                    lgt.Reset = token.Value<Int32>("reset");
                    lgt.ResetInfo = token.Value<Int32>("reset_info");
                    lgt.Temperature = token.Value<double>("temp");
                    lgt.Voltage = token.Value<double>("voltage");
                    lgt.EdgeDeviceId = edge_device_id;
                    lgt.SensorId = token.Value<String>("slcid");

                    Int64 epoch_offset = token.Value<Int64>("timestamp");
                    lgt.SensorTimestamp = UnixTimeStampToDateTime(epoch_offset);

                    if (token.SelectToken("identifier") != null)
                    {
                        lgt.Identifier = token.Value<String>("identifier").ToUpper();
                    }

                    return Repository.AddTelemetryLighting(lgt);
                }
                else
                {
                    if (token.SelectToken("users_connected_count") != null)
                    {
                        TelemetryPublicWiFi pwf = new TelemetryPublicWiFi();
                        pwf.DataDownload = token.Value<double>("data_download");
                        pwf.DataUpload = token.Value<double>("data_upload");
                        pwf.UserConnectedCount = token.Value<Int32>("users_connected_count");
                        pwf.SamplingRate = token.Value<Int32>("sampling_rate");
                        pwf.EdgeDeviceId = edge_device_id;
                        pwf.SensorId = token.Value<String>("mac_address");

                        Int64 epoch_offset = token.Value<Int64>("timestamp");
                        pwf.SensorTimestamp = UnixTimeStampToDateTime(epoch_offset);

                        return Repository.AddTelemetryPublicWifi(pwf);
                    }
                    else
                    {
                        return HttpStatusCode.BadRequest;
                    }
                }
            }
        }

        static private HttpStatusCode HandleAlertPayload(JToken token, String edge_device_id, string clientipaddress)
        {
            string event_source = "null";

            if (token.SelectToken("event_name") != null)
            {
                DeviceEvent devev = new DeviceEvent();
                devev.EventName = token.Value<string>("event_name");
                devev.EventParameter = token.Value<string>("event_parameter");
                devev.EdgeDeviceId = edge_device_id;
                
                if (token.SelectToken("slaveid") != null)
                {
                    devev.SensorId = token.Value<String>("slaveid");
                    event_source = "environment";
                }
                else
                {
                    if (token.SelectToken("slcid") != null)
                    {
                        devev.SensorId = token.Value<String>("slcid");
                        event_source = "lighting";
                    }
                    else
                    {
                        if (token.SelectToken("mac_address") != null)
                        {
                            devev.SensorId = token.Value<String>("mac_address");
                            event_source = "public wifi";
                        }
                        else
                        {
                            devev.SensorId = edge_device_id;
                            event_source = "unknown";
                        }
                    }
                }

                Int64 epoch_offset = token.Value<Int64>("timestamp");
                devev.SensorTimestamp = UnixTimeStampToDateTime(epoch_offset);

                try
                {
                    if (db_connection.State != ConnectionState.Open)
                    {
                        logger.Write("Connection is not open, reopening");
                        db_connection.Open();
                    }

                    string command_string = null;

                    command_string += "INSERT INTO public.event_history (edge_device_id, sensor_id, event_name, event_parameter, sensor_timestamp) VALUES('<edge_device_id>','<sensor_id>', '<event_name>','<event_parameter>', '<sensor_timestamp>', '<event_source>')";

                    command_string = command_string.Replace("<event_name>", devev.EventName);
                    command_string = command_string.Replace("<event_parameter>", devev.EventParameter);

                    command_string = command_string.Replace("<edge_device_id>", devev.EdgeDeviceId);
                    command_string = command_string.Replace("<sensor_id>", devev.SensorId);
                    command_string = command_string.Replace("<event_source>", event_source);
                    command_string = command_string.Replace("<sensor_timestamp>", devev.SensorTimestamp.ToString("yyyy-MM-dd HH:mm:ss"));

                    NpgsqlCommand command = new NpgsqlCommand(command_string);
                    command.Connection = db_connection;
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        logger.Write("Inserted " + event_source + " sensor event");
                    }

                    return HttpStatusCode.NoContent;
                }
                catch (Exception ex)
                {
                    if (ex != null)
                    {
                        if (ex.InnerException != null)
                        {
                        }
                    }
                }

                return HttpStatusCode.BadRequest;
            }
            return HttpStatusCode.BadRequest;
        }
     }
}
