﻿using System;
using System.IO;

namespace iot
{
    public enum Platform
    {
        Windows,
        Linux,
        Mac
    }

    public class SystemInformation
    {
        public SystemInformation()
        {
            OperatingSystemName = RunningPlatform().ToString();
            OperatingSystemVersion = Environment.OSVersion.Version.ToString();

            if (Environment.Is64BitOperatingSystem)
            {
                RegisterSize = "64 Bit";
            }
            else
            {
                RegisterSize = "32 Bit";
            }

            CLRVersion = Environment.Version.ToString();

            if (Type.GetType("Mono.Runtime") != null)
            {
                CLRType = "Mono";
            }
            else
            {
                CLRType = ".NET";
            }

        }

        String _OSName = "Windows";
        String _OSVersion = "10";
        String _RegisterSize = "32 Bit";
        String _CLRType = ".NET";
        String _CLRVersion = "4.0";

        public String OperatingSystemName
        {
            get { return _OSName; }
            set { _OSName = value; }
        }

        public String OperatingSystemVersion
        {
            get { return _OSVersion; }
            set { _OSVersion = value; }
        }

        public String RegisterSize
        {
            get { return _RegisterSize; }
            set { _RegisterSize = value; }
        }

        public String CLRType
        {
            get { return _CLRType; }
            set { _CLRType = value; }
        }

        public String CLRVersion
        {
            get { return _CLRVersion; }
            set { _CLRVersion = value; }
        }

        static Platform RunningPlatform()
        {
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Unix:
                    // Well, there are chances MacOSX is reported as Unix instead of MacOSX.
                    // Instead of platform check, we'll do a feature checks (Mac specific root folders)
                    if (Directory.Exists("/Applications")
                        & Directory.Exists("/System")
                        & Directory.Exists("/Users")
                        & Directory.Exists("/Volumes"))
                        return Platform.Mac;
                    else
                        return Platform.Linux;

                case PlatformID.MacOSX:
                    return Platform.Mac;

                default:
                    return Platform.Windows;
            }
        }
    }
}