﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Reflection;

namespace iot
{
    public class Logger
    {
        static string LogDirectory = null;
        static string DirInprogress = null;
        static string DirComplete = null;
        static Int32 LogFileSizeMB = 10;

        static object ThreadLock = new object();

        public Logger()
        {
            LogDirectory = ConfigurationManager.AppSettings["LogDirectory"];
            LogFileSizeMB = Convert.ToInt32(ConfigurationManager.AppSettings["LogFileSizeMB"]);

            if (Directory.Exists(LogDirectory) == false)
            {
                Directory.CreateDirectory(LogDirectory);
            }

            DirInprogress = LogDirectory + "\\Log\\";
            DirComplete = LogDirectory + "\\Backup\\";

            if (Directory.Exists(DirInprogress) == false)
            {
                Directory.CreateDirectory(DirInprogress);
            }

            if (Directory.Exists(DirComplete) == false)
            {
                Directory.CreateDirectory(DirComplete);
            }
        }

        public void Write(String str)
        {
            lock (ThreadLock)
            {
                String filename_inprogress = DirInprogress + Assembly.GetExecutingAssembly().GetName().Name + ".log";
                String filename_complete = DirComplete + Assembly.GetExecutingAssembly().GetName().Name + DateTime.Now.ToString("yyyyMMddhhmmss") + ".log";

                FileInfo fi = new FileInfo(filename_inprogress);

                if (fi.Exists)
                {
                    if (fi.Length > 1024 * 1024 * LogFileSizeMB)
                    {
                        File.Move(filename_inprogress, filename_complete);
                    }
                }
                File.AppendAllText(filename_inprogress, DateTime.Now.ToString() + " " + str + "\r\n");
            }
        }
    }
}