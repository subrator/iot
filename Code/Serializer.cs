﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace iot
{
    public class Serializer
    {
        public Serializer()
        {

        }

        #region Serialization
        public void SerializeJson<T>(T item, ref String serializedJson)
        {
            serializedJson = JsonConvert.SerializeObject(item);
        }

        public void SerializeXml<T>(T item, ref String serializedXml)
        {
            var xmlserializer = new XmlSerializer(typeof(T));
            var stringWriter = new StringWriter();
            using (var writer = XmlWriter.Create(stringWriter))
            {
                xmlserializer.Serialize(writer, item);
                serializedXml = stringWriter.ToString();
            }
        }

        public void DeSerializeJson<T>(ref T item, ref String serializedJson)
        {
            item = JsonConvert.DeserializeObject<T>(serializedJson);
        }
        public void DeSerializeXml<T>(ref T item, ref String serializedXml)
        {
            MemoryStream stream = new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(serializedXml));

            XmlSerializer serializer = new XmlSerializer(typeof(T));
            item = (T)serializer.Deserialize(stream);
        }
        #endregion
    }
}