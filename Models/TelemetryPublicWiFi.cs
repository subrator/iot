﻿using System;
using System.Runtime.Serialization;

namespace iot.Model
{
    [DataContract]
    class TelemetryPublicWiFi : Object
    {
        public TelemetryPublicWiFi()
        {
        }

        [DataMember]
        public virtual DateTime SensorTimestamp { get; set; }

        [DataMember]
        public virtual Double DataDownload { get; set; }

        [DataMember]
        public virtual Double DataDownloadMB { get; set; }

        [DataMember]
        public virtual Double DataDownloadMBPS { get; set; }

        [DataMember]
        public virtual Double DataUpload { get; set; }

        [DataMember]
        public virtual Double DataUploadMB { get; set; }
        [DataMember]

        public virtual Double DataUploadMBPS { get; set; }

        [DataMember]
        public virtual Int32 UserConnectedCount { get; set; }

        [DataMember]
        public virtual String EdgeDeviceId { get; set; }

        [DataMember]
        public virtual String SensorId { get; set; }

        [DataMember]
        public virtual Int32 SamplingRate { get; set; }
    }
}
