﻿using System;
using System.Runtime.Serialization;

namespace iot.Model
{
    [DataContract]
    public class Peripheral : Object
    {
        public Peripheral()
        {
        }

        [DataMember]
        public virtual String DeviceId { get; set; }

        [DataMember]
        public virtual String PeripheralId { get; set; }

        [DataMember]
        public virtual Int32 PeripheralTypeId { get; set; }

        [DataMember]
        public virtual String PeripheralName { get; set; }

        [DataMember]
        public virtual String IoType { get; set; }

        [DataMember]
        public virtual String PeripheralType { get; set; }
    }
 }
