﻿using System;
using System.Runtime.Serialization;

namespace iot.Model
{
    [DataContract]
    class TelemetryEnvironment : Object
    {
        public TelemetryEnvironment()
        {
        }

        [DataMember]
        public virtual DateTime SensorTimestamp { get; set; }

        [DataMember]
        public virtual Double PM10 { get; set; }

        [DataMember]
        public virtual Double PM25 { get; set; }

        [DataMember]
        public virtual Double CO2 { get; set; }

        [DataMember]
        public virtual Double NO2 { get; set; }

        [DataMember]
        public virtual Double SO2 { get; set; }

        [DataMember]
        public virtual Double O3 { get; set; }

        [DataMember]
        public virtual Double CO { get; set; }

        [DataMember]
        public virtual Double DewPoint { get; set; }

        [DataMember]
        public virtual Double RelativeHumidity { get; set; }

        [DataMember]
        public virtual Double Temperature { get; set; }

        [DataMember]
        public virtual String EdgeDeviceId { get; set; }

        [DataMember]
        public virtual String SensorId { get; set; }
    }
}
