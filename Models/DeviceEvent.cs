﻿using System;
using System.Runtime.Serialization;

namespace iot.Model
{
    [DataContract]
    public class DeviceEvent: Object
    {
        [DataMember]
        public virtual DateTime SensorTimestamp { get; set; }

        [DataMember]
        public virtual String EdgeDeviceId { get; set; }

        [DataMember]
        public virtual String SensorId { get; set; }

        [DataMember]
        public virtual String EventName { get; set; }

        [DataMember]
        public virtual String EventParameter { get; set; }

        [DataMember]
        public virtual String EventSource { get; set; }
    }
}