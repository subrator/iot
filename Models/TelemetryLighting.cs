﻿using System;
using System.Runtime.Serialization;

namespace iot.Model
{
    [DataContract]
    class TelemetryLighting : Object
    {
        public TelemetryLighting()
        {
        }

        [DataMember]
        public virtual DateTime SensorTimestamp { get; set; }

        [DataMember]
        public virtual Int32 AnalogInput { get; set; }

        [DataMember]
        public virtual Double BurnHours { get; set; }

        [DataMember]
        public virtual Double Current { get; set; }

        [DataMember]
        public virtual Int32 Db { get; set; }

        [DataMember]
        public virtual Int32 DigitalInput { get; set; }

        [DataMember]
        public virtual Double Dimming { get; set; }

        [DataMember]
        public virtual Double EnergyKwh { get; set; }

        [DataMember]
        public virtual Int32 LampMode { get; set; }

        [DataMember]
        public virtual Double LampSteadyCurrent { get; set; }

        [DataMember]
        public virtual Double PowerFactor { get; set; }

        [DataMember]
        public virtual Double PowerWatts { get; set; }

        [DataMember]
        public virtual Int32 Reset { get; set; }

        [DataMember]
        public virtual Int32 ResetInfo { get; set; }

        [DataMember]
        public virtual Double Temperature { get; set; }

        [DataMember]
        public virtual Double Voltage { get; set; }

        [DataMember]
        public virtual String EdgeDeviceId { get; set; }

        [DataMember]
        public virtual Int32 Di01 { get; set; }

        [DataMember]
        public virtual Int32 Di02 { get; set; }

        [DataMember]
        public virtual Int32 Di03 { get; set; }

        [DataMember]
        public virtual Int32 Di04 { get; set; }

        [DataMember]
        public virtual Int32 Di05 { get; set; }

        [DataMember]
        public virtual Int32 Di06 { get; set; }

        [DataMember]
        public virtual Int32 Di07 { get; set; }

        [DataMember]
        public virtual Int32 Di08 { get; set; }

        [DataMember]
        public virtual Int32 Di09 { get; set; }

        [DataMember]
        public virtual Int32 Di10 { get; set; }

        [DataMember]
        public virtual Int32 Di11 { get; set; }

        [DataMember]
        public virtual Int32 Di12 { get; set; }

        [DataMember]
        public virtual Int32 Di13 { get; set; }

        [DataMember]
        public virtual Int32 Di14 { get; set; }

        [DataMember]
        public virtual Int32 Di15 { get; set; }

        [DataMember]
        public virtual Int32 Di16 { get; set; }

        [DataMember]
        public virtual String SensorId { get; set; }

        [DataMember]
        public virtual String Identifier { get; set; }
    }
}
