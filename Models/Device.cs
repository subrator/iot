﻿using System;
using System.Runtime.Serialization;

namespace iot.Model
{
    [DataContract]
    public class Device : Object
    {
        public Device()
        {
        }

        [DataMember]
        public virtual String DeviceId { get; set; }

        [DataMember]
        public virtual String Location { get; set; }

        [DataMember]
        public virtual String DeviceName { get; set; }

        [DataMember]
        public virtual Double Latitude { get; set; }

        [DataMember]
        public virtual Double Longitude { get; set; }

        [DataMember]
        public virtual String ActiveIpAddress { get; set; }

        [DataMember]
        public virtual Boolean IsDeleted { get; set; }
    }

}
