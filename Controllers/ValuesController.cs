﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using iot.Model;
using System.Threading.Tasks;
using System.Web;

namespace iot.Controllers
{
    public class iotController : ApiController
    {
        private static Repository repo = null;
        Serializer serializer = new Serializer();
        public iotController()
        {
            if(repo == null)
            {
                repo = new Repository();
            }

            if(repo.IsOpen == false)
            {
                string statstr = null;

                repo.Open(ref statstr);
            }
        }

        private HttpResponseMessage GetResponse<T>(ref T obj, HttpStatusCode stat = HttpStatusCode.OK)
        {
            var resp = new HttpResponseMessage(stat);

           if (obj != null)
            {
                String val = "";
                serializer.SerializeJson<T>(obj, ref val);
                resp.Content = new StringContent(val, System.Text.Encoding.UTF8, "text/plain");
            }

            return resp;
        }

        [Route("ping")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            SystemInformation sys = new SystemInformation();
            return GetResponse<SystemInformation>(ref sys);
        }

        [Route("telemetry/environment")]
        [Route("telemetry/1")]
        [HttpGet]
        public HttpResponseMessage GetEnvironmentTelememtry(string deviceid = null, Int32 pagesize = 10, Int32 pageno = 1)
        {
            TelemetryEnvironment[] env = repo.GetTelemetryEnvironment(deviceid, pagesize, pageno);
            return GetResponse<TelemetryEnvironment[]>(ref env); 
        }

        [Route("telemetry/lighting")]
        [Route("telemetry/2")]
        [HttpGet]
        public HttpResponseMessage GetLightingTelememtry(string deviceid = null, Int32 pagesize = 10, Int32 pageno = 1)
        {
            TelemetryLighting[] lgt = repo.GetTelemetryLighting(deviceid, pagesize, pageno);
            return GetResponse<TelemetryLighting[]>(ref lgt);
        }

        [Route("telemetry/publicwifi")]
        [Route("telemetry/3")]
        [HttpGet]
        public HttpResponseMessage GetPublicWiFiTelememtry(string deviceid = null, Int32 pagesize = 10, Int32 pageno = 1)
        {
            TelemetryPublicWiFi[] pwf = repo.GetTelemetryPublicWiFi(deviceid, pagesize, pageno);
            return GetResponse<TelemetryPublicWiFi[]>(ref pwf);
        }

        [Route("deviceevent")]
        [HttpGet]
        public HttpResponseMessage GetDeviceEvent(string deviceid = null, Int32 pagesize = 10, Int32 pageno = 1)
        {
            DeviceEvent[] evt = repo.GetDeviceEvents(deviceid, pagesize, pageno);
            return GetResponse<DeviceEvent[]>(ref evt);
        }

        [Route("device")]
        [HttpGet]
        public HttpResponseMessage GetDevice(string clientid = null, Int32 pagesize = 10, Int32 pageno = 1)
        {
            Device[] devarr = repo.GetDevices(clientid, pagesize, pageno);
            return GetResponse<Device[]>(ref devarr);
        }

        [Route("deviceperipheral")]
        [HttpGet]
        public HttpResponseMessage GetPeripheral(string deviceid = null, Int32 pagesize = 10, Int32 pageno = 1)
        {
            Peripheral[] prlarr = repo.GetPeripherals(deviceid, pagesize, pageno);
            return GetResponse<Peripheral[]>(ref prlarr);
        }

        [Route("telemetry")]
        public async Task PostTelemetryAsync()
        {
            string clientAddress = HttpContext.Current.Request.UserHostAddress;
            string value = await Request.Content.ReadAsStringAsync();
            Repository.HandlePost(value, clientAddress);
        }

        [Route("alert")]
        public async Task PostAlert()
        {
            string clientAddress = HttpContext.Current.Request.UserHostAddress;
            string value = await Request.Content.ReadAsStringAsync();
            Repository.HandleAlert(value, clientAddress);
        }

        [Route("common")]
        public async Task PostCommon()
        {
            string value = await Request.Content.ReadAsStringAsync();
        }

        [Route("cmd_query")]
        public async Task PostCommand()
        {
            string value = await Request.Content.ReadAsStringAsync();
        }

        [Route("notification")]
        public async Task PostNotification()
        {
            string value = await Request.Content.ReadAsStringAsync();
        }

        [Route("response")]
        public async Task PostResponse()
        {
            string value = await Request.Content.ReadAsStringAsync();
        }
    }
}
